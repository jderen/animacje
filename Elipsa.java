package animacjee;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Elipsa extends Figura {
	 private Graphics2D buffer;
     // rozmiary pulpitu
 private int width, height;
 private int delay;    

 private Color clr;
     // do transformacji
 private Area area;
     // do wykreslania
 private Shape shape;
     // przeksztalcenie obiektu
 private AffineTransform aft;    
     // przesuniecie
 private int dx, dy;
     // rozciaganie
 private double sf;
     // kat obrotu
 private double an;
     // ziarno dla generatora liczb losowych
 static private int seed = 0;
	public Elipsa(Graphics2D buffer, int delay, int width, int height) {
		super(buffer,delay,width,height);
	
	}

}